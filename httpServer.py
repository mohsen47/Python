import BaseHTTPServer
import sys
import signal
import json
import thread
import ConfigParser
import os

from git import Repo

def signal_handler(signal, frame):
    print("Done exiting now")
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

class MyRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    configFileName = "options.conf"
    config = ConfigParser.ConfigParser()
    try:
        config.readfp(open(configFileName))
    except Exception as e:
        print("Configuration file {} does not exist".format(configFileName))
        sys.exit(1)
    options = None
    if not config.has_section("repository"):
        print("Section repository not found")
        sys.exit(1)
    repoPath = None
    workTree = None
    branch = None
    try:
        repoPath = config.get('repository','git_dir')
        workTree = config.get('repository','work_tree')
        branch = config.get('repository','branch')
    except Exception as e:
        print("Options git_dir and/or work_tree and/or branch do not exist")
        sys.exit(1)
    repo = Repo(repoPath)
    try:
        repo.heads[branch].checkout()
    except Exception as e:
        pass
    origin = repo.remotes['origin']

    def pullOrigin(self):
        os.environ["GIT_DIR"] = self.repoPath
        os.environ["GIT_WORK_TREE"] = self.workTree
        git = MyRequestHandler.repo.git
        git.fetch("origin")
        git.reset("--hard", "origin/" + self.branch)

    def do_GET(self):
        self.send_response(303)
        self.end_headers()
    def do_POST(self):
        size = int(self.headers['Content-Length'])
        data = self.rfile.read(size)
        jsonData = json.loads(data)
        eventType = self.headers['X-Gitlab-Event']
        try:
            if eventType == "Push Hook":
                ref = jsonData['ref']
                ref = ref.split("/")[2]
                if ref == MyRequestHandler.branch:
                    self.pullOrigin()

        except Exception as e:
            print(e)
        self.send_response(200)
        self.end_headers()

server_address = ('',8000)

httpd = BaseHTTPServer.HTTPServer(server_address, MyRequestHandler)
try:
    httpd.serve_forever()
except Exception as e:
    sys.exit(0)
